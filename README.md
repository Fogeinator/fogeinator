# hewwo 👋👁‍🗨👄👁‍🗨✨ 

**hey** you, 

###### yes, you 

just wanted to tell you you're 

### ✨ _`awesome`_ ✨

because you took the time to come to my profile 🥺

#### that said, here's some stuff: 
- 🔭 Working on: finishing school 🏫
- 🔧 Current project: a calculator 🧮 and an alarm clock ⏰
- 🌱 Learning:  Full Stack Web Dev and Pythonistic stuff 🐍
- 🤔 Thinking about: epic side projects to do 😎 
- 💬 Ask me about: my day and I'll ask about yours 😉
- 💖 I love: emojis 👀

## 📧📮✉📨📩📫💌📪📬📭
| 🕸 | find me at: |
|---|---|
| 📧 | [ongzhizheng@gmail.com](mailto:ongzhizheng@gmail.com) |
| 💌 | [spam me!](mailto:hewwo@ongzz.me) |
| 📷 | [@ong.zhi.zheng](https://instagram.com/ong.zhi.zheng) |
| 📘 | [Ong Zhi Zheng](https://www.facebook.com/profile.php?id=100009737623508) |
| 🐤 | [@ongzzzzzz](https://twitter.com/ongzzzzzz) |
| 💻 | [epic website](http://ongzz.me) |

## 📈📉📊📉📈📉📊📉📈📉📊
![the thing everyone puts on profile readme](https://github-readme-stats.vercel.app/api?username=Fogeinator&show_icons=true&count_private=true&theme=dracula)
